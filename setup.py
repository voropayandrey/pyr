#!/usr/bin/env python

from distutils.core import setup

def readme():
    with open('README.md') as f:
        return f.read()

setup(name='PyR',
      version='0.0.1',
      description='Implementation of a R peak detector for ECG signals',
      long_description=readme(),
      classifiers=[],
      author='Voropai Andrii',
      author_email='voropayandrey@gmail.com',
      packages=[''],
      requires=['numpy', 'scipy', 'math'])