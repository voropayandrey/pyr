
import numpy as np
import sys
import matplotlib
import matplotlib.pyplot as plt
import os

import pyr.pyr_ecg
from pyr.utils import *
from pyr.pyr_ecg import PyR
from pyr.data_frames.ecg_data_frame import ECGDataFrame
from pyr.data_sources.simple_csv_data_source import SimpleCSVDataSource

ROOT_DIR = get_project_root()
Fs = 1000
offset = 0
end = Fs * 0
file_path = ROOT_DIR + '/data/020521/20210502130524.csv'

def test_detector():
    df = ECGDataFrame(data_source=SimpleCSVDataSource(path_to_file=file_path,
                                                      offset_samples=offset,
                                                      end_samples=end,
                                                      csv_column=0),
                      sample_frequency=Fs,
                      reference_voltage_mv=5000,
                      adc_resolution_bits=24,
                      gain=20,
                      filter_low_hz=10,
                      filter_high_hz=30)

    detector = PyR(data_frame=df,
                   history_enabled=True,
                   backsearch_enabled=True,
                   snr_enabled=True)
    detector.detect_all()
    intervals = detector.get_intervals()
    assert len(intervals) >= 322
    assert len(intervals) <= 326
    snr = detector.get_snr_total()
    assert snr > 7.0
    assert snr < 9.0

if __name__ == "__main__":

    print("Python version: ", sys.version)
    print("Matplotlib backend: " + plt.get_backend())
    matplotlib.use('Qt5Agg')
    print("Matplotlib backend switched to: " + plt.get_backend())
    df = ECGDataFrame(data_source=SimpleCSVDataSource(path_to_file=file_path,
                                                      offset_samples=offset,
                                                      end_samples=end,
                                                      csv_column=0),
                      sample_frequency=Fs,
                      reference_voltage_mv=5000,
                      adc_resolution_bits=24,
                      gain=20,
                      filter_low_hz=10,
                      filter_high_hz=30)

    detector = PyR(data_frame=df,
                   history_enabled=True,
                   backsearch_enabled=True,
                   snr_enabled=True)
    detector.detect_all()
    intervals = detector.get_intervals()
    total_snr = detector.get_snr_total()
    print("total R-peak count:", len(intervals) + 1)
    print("total_snr: ", total_snr)
    detector.plot()
