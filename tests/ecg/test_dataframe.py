import os
import sys

import pytest

dir_path = os.path.dirname(os.path.realpath(__file__))
parent_dir_path = os.path.abspath(os.path.join(dir_path, os.pardir))
sys.path.insert(0, parent_dir_path)
from pyr.pyr_ecg import PyR
from pyr.data_frames.ecg_data_frame import ECGDataFrame
from pyr.data_sources.simple_csv_data_source import SimpleCSVDataSource

def test_dataframe_raw_ecg():
    df = ECGDataFrame(data_source=SimpleCSVDataSource(path_to_file='data/230321/2021-03-23T17:12:57Z-450072.csv',
                                                      csv_column=0),
                      sample_frequency=500)

    rawECG = df.get_samples()

    totalItemsCount = len(rawECG)

    assert totalItemsCount == 450072
    assert rawECG[0] == 129
    assert rawECG[450071] == -63

def test_dataframe_raw_ecg_offset5_end10():
    df = ECGDataFrame(data_source=SimpleCSVDataSource(path_to_file='data/230321/2021-03-23T17:12:57Z-450072.csv',
                                                      offset_samples=5,
                                                      end_samples=10,
                                                      csv_column=0),
                      sample_frequency=500)

    rawECG = df.get_samples()
    totalItemsCount = len(rawECG)

    assert totalItemsCount == 5
    assert rawECG[0] == 110
    assert rawECG[4] == 95

def test_dataframe_rr():

    df = ECGDataFrame(data_source=SimpleCSVDataSource(path_to_file='data/230321/rr/RR_AndreyCall2021-03-23T17:12:57Z-450072.csv'),
                      sample_frequency=500)
    rr = df.get_samples()

    totalItemsCount = len(rr)

    assert totalItemsCount == 1354
    assert rr[0] == 698
    assert rr[1353] == 680