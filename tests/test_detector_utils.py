import os
import sys

dir_path = os.path.dirname(os.path.realpath(__file__))
parent_dir_path = os.path.abspath(os.path.join(dir_path, os.pardir))
sys.path.insert(0, parent_dir_path)

import pyr.utils

def test_detector_utils_1():
    array = [0, 100, 200, 300, 400, -500, 650, 700, -800]
    assert pyr.utils.find_value_in(array, 300) == True

def test_detector_utils_2():
    array = [0, 100, 200, 300, 400, -500, 650, 700, -800]
    assert pyr.utils.find_value_in(array, 50, range=50) == True

def test_detector_utils_3():
    array = [0, 100, 200, 300, 400, -500, 650, 700, -800]
    assert pyr.utils.find_value_in(array, 1000, range=100) == False

def test_detector_utils_4():
    array = [0, 100, 200, 300, 400, -500, 650, 700, -800]
    assert pyr.utils.find_value_in(array, -490, range=10) == True

def test_detector_utils_5():
    array = [0, 100, 200, 300, 400, -500, 650, 700, -800]
    assert pyr.utils.find_value_in(array, -791, range=9) == True

def test_detector_utils_5():
    array = [0, 100, 200, 300, 400, -500, 650, 700, -800]
    assert pyr.utils.find_value_in(array, -791, range=10) == False

def test_detector_utils_6():
    array = [-20, -30, -40, 1, 20, 30]
    assert pyr.utils.find_value_in(array, -4, range=5) == True

def test_detector_utils_7():
    array = [-20, -30, -40, 1, 20, 30]
    assert pyr.utils.find_value_in(array, -4, range=4) == False