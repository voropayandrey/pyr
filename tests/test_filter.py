import pyr.pyr_ecg
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import freqz
import pytest

def generate_noise(noise_frequency, sample_frequency, duration):
    number_of_samples = duration * sample_frequency
    t = np.linspace(0, duration, int(number_of_samples), endpoint=False)
    return np.cos(2 * np.pi * noise_frequency * t)

def test_filter_fs500_fcut21Hz():
    fs = 500
    T = 1.0
    number_of_samples = fs * T
    x = generate_noise(21, fs, T)
    filter = pyr.pyr_ecg.ButterworthFilter(sample_frequency=500, filter_order=5, frequency_low=5, frequency_high=21)
    y = filter.filter_all(x)
    np_fft_x = np.fft.fft(x)
    np_fft_y = np.fft.fft(y)
    amplitudes_x = 2 / number_of_samples * np.abs(np_fft_x)
    amplitudes_y = 2 / number_of_samples * np.abs(np_fft_y)
    amplitude_x_max = max(amplitudes_x)
    amplitude_y_max = max(amplitudes_y)
    assert amplitude_x_max >= 1.0
    assert amplitude_y_max > 0.6
    assert amplitude_y_max < 0.7

def test_filter_fs500_fcut5Hz():
    fs = 500
    T = 1.0
    number_of_samples = fs * T
    x = generate_noise(5, fs, T)
    filter = pyr.pyr_ecg.ButterworthFilter(sample_frequency=500, filter_order=5, frequency_low=5, frequency_high=21)
    y = filter.filter_all(x)
    np_fft_x = np.fft.fft(x)
    np_fft_y = np.fft.fft(y)
    amplitudes_x = 2 / number_of_samples * np.abs(np_fft_x)
    amplitudes_y = 2 / number_of_samples * np.abs(np_fft_y)
    amplitude_x_max = max(amplitudes_x)
    amplitude_y_max = max(amplitudes_y)
    assert amplitude_x_max >= 1.0
    assert amplitude_y_max > 0.5
    assert amplitude_y_max < 0.7

def test_filter_fs1000_fcut21Hz():
    fs = 1000
    T = 1.0
    number_of_samples = fs * T
    x = generate_noise(21, fs, T)
    filter = pyr.pyr_ecg.ButterworthFilter(sample_frequency=1000, filter_order=5, frequency_low=5, frequency_high=21)
    y = filter.filter_all(x)
    np_fft_x = np.fft.fft(x)
    np_fft_y = np.fft.fft(y)
    amplitudes_x = 2 / number_of_samples * np.abs(np_fft_x)
    amplitudes_y = 2 / number_of_samples * np.abs(np_fft_y)
    amplitude_x_max = max(amplitudes_x)
    amplitude_y_max = max(amplitudes_y)
    assert amplitude_x_max >= 1.0
    assert amplitude_y_max > 0.5
    assert amplitude_y_max < 0.7

def test_filter_fs1000_fcut5Hz():
    fs = 1000
    T = 1.0
    number_of_samples = fs * T
    x = generate_noise(5, fs, T)
    filter = pyr.pyr_ecg.ButterworthFilter(sample_frequency=1000, filter_order=5, frequency_low=5, frequency_high=21)
    y = filter.filter_all(x)
    np_fft_x = np.fft.fft(x)
    np_fft_y = np.fft.fft(y)
    amplitudes_x = 2 / number_of_samples * np.abs(np_fft_x)
    amplitudes_y = 2 / number_of_samples * np.abs(np_fft_y)
    amplitude_x_max = max(amplitudes_x)
    amplitude_y_max = max(amplitudes_y)
    assert amplitude_x_max >= 1.0
    assert amplitude_y_max > 0.5
    assert amplitude_y_max < 0.7

def test_filter_fs1000_fcut5Hz_steps():
    fs = 1000
    T = 1.0
    number_of_samples = fs * T
    x = generate_noise(5, fs, T)
    filter = pyr.pyr_ecg.ButterworthFilter(sample_frequency=1000, filter_order=5, frequency_low=5, frequency_high=21)
    y = np.zeros(int(number_of_samples))
    for index, value in enumerate(x):
        filtered_sample = filter.filter_sample(value)
        y[index] = filtered_sample

    np_fft_x = np.fft.fft(x)
    np_fft_y = np.fft.fft(y)
    amplitudes_x = 2 / number_of_samples * np.abs(np_fft_x)
    amplitudes_y = 2 / number_of_samples * np.abs(np_fft_y)
    amplitude_x_max = max(amplitudes_x)
    amplitude_y_max = max(amplitudes_y)
    assert amplitude_x_max >= 1.0
    assert amplitude_y_max > 0.5
    assert amplitude_y_max < 0.7

if __name__ == "__main__":
    fs = 500.0
    T = 1.00
    nsamples = T * fs
    t = np.linspace(0, T, int(nsamples), endpoint=False)
    a = 0.02
    f0 = 50.0
    #x = 0.1 * np.sin(2 * np.pi * 1.0 * np.sqrt(t))
    x = 0.01 * np.cos(2 * np.pi * 100 * t + 0.1)
    x += a * np.cos(2 * np.pi * f0 * t + .11)
    x += 0.03 * np.cos(2 * np.pi * 50 * t)
    filter = pyr.pyr_ecg.ButterworthFilter()
    b, a = filter.get_coefficients(500, 5)
    y = filter.filter_all(x)
    w, h = freqz(b, a, worN=500)

    plt.figure(1)
    plt.clf()
    plt.plot((fs * 0.5 / np.pi) * w, abs(h), label="order = 5")
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Gain')
    plt.grid(True)
    plt.legend(loc='best')

    plt.figure(2)
    plt.clf()
    plt.plot(t, x, label='Noisy signal')
    plt.plot(t, y, label='Filtered signal (%g Hz)' % f0)
    plt.xlabel('time (seconds)')
    #plt.hlines([-a, a], 0, T, linestyles='--')
    plt.grid(True)
    plt.axis('tight')
    plt.legend(loc='upper left')

    plt.show()