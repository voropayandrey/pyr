#!/usr/bin/env python

import sys
import argparse
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pyr.pyr_ecg
import os
from pyr.pyr_ecg import PyR
from pyr.data_frames.ecg_data_frame import ECGDataFrame
from pyr.data_sources.simple_csv_data_source import SimpleCSVDataSource

parser = argparse.ArgumentParser(description="Let me detect R-Peaks for your.")
parser.add_argument("INPUT_FILE_PATH", type=str, help="A CSV file of raw ECG data.")
parser.add_argument("--output_folder", dest="OUTPUT_FOLDER_PATH", type=str, help="Set a path of output file with RR intervals.")
parser.add_argument("--output_name", dest="OUTPUT_FILE_NAME", type=str, help="Name of output file.")
parser.add_argument("--verbose", dest="IS_VERBOSE", default=False, action="store_true", help="Print an addition information.")
parser.add_argument("--plot", dest="DEBUG_PLOT", default=False, action="store_true", help="Plot debug graphs.")
parser.add_argument("--from", dest="FROM_SECONDS", default=0, type=int, help="Read file from seconds.")
parser.add_argument("--to", dest="TO_SECONDS", default=0, type=int, help="Read file to seconds.")
parser.add_argument("--fs", dest="SAMPLE_FREQUENCY", default=0, type=int, help="Sample frequency of recorded input file.")
parser.add_argument("--adc_vr", dest="REFERENCE_ADC_VOLTAGE", default=1000, type=int, help="Reference ADC voltage of recorded input file.")
parser.add_argument("--adc_bits", dest="ADC_RESOLUTION_BITS", default=16, type=int, help="ADC resolution in bits of recorded input file.")
parser.add_argument("--adc_gain", dest="ADC_GAIN", default=1, type=int, help="Gain of amplifier of recorded input file.")
parser.add_argument("--channel", dest="DATA_CHANNEL", default=0, type=int, help="data channel of recorded input file.")
parser.add_argument("--threshold", dest="DETECTOR_THRESHOLD", default=80, type=int, help="Threshold level in percents for R-R detection")
parser.add_argument("--invert", dest="INVERT", default=False, action="store_true", help="Invert signal.")
parser.add_argument("--backsearch", dest="BACKSEARCH", default=False, action="store_true", help="Invert signal.")
parser.add_argument("--filter_from", dest="PRE_FILTER_FROM_HZ", default=5, type=int, help="Set low frequency for a band-pass pre-filter for the input signal.")
parser.add_argument("--filter_to", dest="PRE_FILTER_TO_HZ",  default=40, type=int, help="Set high frequency for a band-pass pre-filter for the input signal.")
args = parser.parse_args()

isVerbose = args.IS_VERBOSE

def v(*args):
    if isVerbose:
        print(*args)

# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    v("Python version: ", sys.version)

    if args.DEBUG_PLOT:
        v("Matplotlib backend: " + plt.get_backend())
        matplotlib.use('Qt5Agg')
        v("Matplotlib backend switched to: " + plt.get_backend())

    inputFiles = args.INPUT_FILE_PATH
    outputFolder = args.OUTPUT_FOLDER_PATH
    outputFileName = args.OUTPUT_FILE_NAME
    fs = args.SAMPLE_FREQUENCY

    filesToProcess = list()
    isFile = os.path.isfile(inputFiles)
    isDir = os.path.isdir(inputFiles)
    if isDir:
        for file in os.listdir(inputFiles):
            isCSV = file.find('.csv') > 0
            isFile = not os.path.isdir(inputFiles + '/' + file)
            if isFile and isCSV:
                filesToProcess.append(inputFiles + file)
    else:
        filesToProcess.append(inputFiles)

    v("Files to process: ", filesToProcess)

    for inputFile in filesToProcess:
        print("Process file: ", inputFile)

        df = ECGDataFrame(data_source=SimpleCSVDataSource(path_to_file=inputFile,
                                                          offset_samples=fs * args.FROM_SECONDS,
                                                          end_samples=fs * args.TO_SECONDS,
                                                          csv_column=args.DATA_CHANNEL),
                          sample_frequency=fs,
                          reference_voltage_mv=args.REFERENCE_ADC_VOLTAGE,
                          adc_resolution_bits=args.ADC_RESOLUTION_BITS,
                          gain=args.ADC_GAIN,
                          invert=args.INVERT,
                          filter_low_hz=args.PRE_FILTER_FROM_HZ,
                          filter_high_hz=args.PRE_FILTER_TO_HZ)

        detector = PyR(data_frame=df,
                            history_enabled=True,
                            backsearch_enabled=args.BACKSEARCH,
                            snr_enabled=True,
                            threshold_height_percent=args.DETECTOR_THRESHOLD)

        detector.detect_all()
        intervals = detector.get_intervals()
        total_snr = detector.get_snr_total()

        v("total_snr: ", total_snr)

        extensionIndex = inputFile.rindex('.')
        folderSlashIndex = inputFile.rindex('/') + 1
        name = inputFile[folderSlashIndex:extensionIndex]
        if outputFolder != None:
            if args.OUTPUT_FILE_NAME != None:
                outputFileName = outputFolder + "/" + args.OUTPUT_FILE_NAME + ".csv"
            else:
                outputFileName = outputFolder + name + "_RR.csv"

            v("Output file path: ", outputFileName)

            intervalsLength = len(intervals)
            if outputFolder != None:
                with open(outputFileName, 'w') as of:
                    csvStringCounter = 0
                    for i in range(intervalsLength):
                        csvString = str(intervals[i])
                        if (csvStringCounter != intervalsLength - 1):
                            csvString += "\n"
                        csvStringCounter += 1
                        of.write(csvString)
                    of.close()

        if args.DEBUG_PLOT:
            detector.plot()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
