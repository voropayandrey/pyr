from pathlib import Path
import os

def find_value_in(array, value, range=0) -> bool:
    for v in array:
        if v >= 0:
            if value >= 0:
                # v = 5
                # value = 10
                # range = 5
                # value - 5 = 5
                # value + 5 = 15
                v_from = value - range
                v_to = value + range
                if v >= v_from and v <= v_to:
                    return True
            else:
                # v = 1
                # value = -4
                # range = 5
                # value + 3 = 1
                # value - 3 = -9
                v_from = value - range
                v_to = value + range
                if v >= v_from and v <= v_to:
                    return True
        else:
            # TODO: Add more tests for negative values
            if value > 0:
                # v = -1
                # value = 4
                # range = 5
                # value + 5 = 1
                # value - 5 = -9
                v_from = value - range
                v_to = value + range
                if v <= v_from and v >= v_to:
                    return True
            else:
                # v = -1
                # value = -4
                # range = 5
                # value + 5 = 1
                # value - 5 = -9
                v_from = value - range
                v_to = value + range
                if v <= v_from and v >= v_from:
                    return True
    return False

def get_project_root() -> str:
    return os.fspath(Path(__file__).parent.parent.absolute())