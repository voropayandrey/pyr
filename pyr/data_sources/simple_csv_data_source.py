from .data_source_interface import ECGDataSourceInterface
import numpy as np
import csv

class SimpleCSVDataSource(ECGDataSourceInterface):
    def __init__(self, path_to_file: str,
                 offset_samples: int = 0,
                 end_samples: int = 0,
                 csv_column: int = 0):
        self.path_to_file = path_to_file
        self.data_offset = offset_samples
        self.data_end = end_samples
        self.channel = csv_column
        self.samples = list()
        
    def read_data(self) -> np.array:
        if self.path_to_file != None and len(self.path_to_file) > 0:
            with open(self.path_to_file, newline='', mode='r') as csvfile:
                reader = csv.reader(csvfile, delimiter=',', quotechar=',')
                is_row_to_append = True
                index = 0
                for row in reader:
                    if self.data_offset != 0 or self.data_end != 0:
                        if index >= self.data_offset:
                            if index < self.data_end:
                                is_row_to_append = True
                            else:
                                is_row_to_append = False
                                break
                        else:
                            is_row_to_append = False

                    if is_row_to_append:
                        if len(row) == 1:
                            amplitude = float(row[0])
                        else:
                            amplitude = float(row[self.channel])
                        self.samples.append(amplitude)
                    index += 1
                csvfile.close()
            return np.array(self.samples)
        else:
            raise FileNotFoundError('Invalid file. Check the file path.')
