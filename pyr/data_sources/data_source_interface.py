from abc import ABC, abstractmethod
import numpy


class ECGDataSourceInterface(ABC):

    @abstractmethod
    def read_data(self) -> numpy.array:
        pass
