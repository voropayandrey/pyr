import csv
from scipy.signal import butter, lfilter, lfilter_zi, bode
import numpy as np
import math
import pyr.utils
from .filters.filter_interface import FilterInterface
from .filters.butterworth_filter import ButterworthFilter
from .data_frames.data_frame_interface import DataFrameInterface
from .detectors.detector_interface import PeakDetectorInterface
from .detectors.modified_pan_tompkins_detector import ModifiedPanTompkinsDetector
import time

class PyR:
    def __init__(self,
                 data_frame: DataFrameInterface,
                 window_width_percent=10,
                 mean_window_width_percent=200,
                 threshold_height_percent=50,
                 debounce_percent=20,
                 backsearch_enabled=False,
                 history_enabled=False,
                 snr_enabled=False,
                 minimal_snr=4.0):
        self.data_frame = data_frame
        self.ecg_samples = self.data_frame.get_samples()
        modified_pan_tompkins_detector = ModifiedPanTompkinsDetector(data_frame=data_frame,
                                                                  window_width_percent=window_width_percent,
                                                                  mean_window_width_percent=mean_window_width_percent,
                                                                  threshold_height_percent=threshold_height_percent,
                                                                  debounce_percent=debounce_percent,
                                                                  backsearch_enabled=backsearch_enabled,
                                                                  history_enabled=history_enabled,
                                                                  snr_enabled=snr_enabled,
                                                                  minimal_snr=minimal_snr,
                                                                  data_length=len(self.ecg_samples))

        self.detectors = list()
        self.detectors.append(modified_pan_tompkins_detector)
        self.processing_time_ms = 0

    def detect_sample(self, sample):
        for detector in self.detectors:
            detector.detect_sample(sample)

    def detect_all(self):
        start_time = time.time_ns()
        for sample in self.ecg_samples:
            self.detect_sample(sample)
        end_time = time.time_ns()
        self.processing_time_ms = (end_time - start_time)/1000000
        # join detected peaks if needed

    def get_intervals(self):
        return self.detectors[0].get_intervals()

    def get_snr_total(self):
        return self.detectors[0].get_snr_total()

    def plot(self):
        self.detectors[0].plot()


