from scipy.signal import butter, lfilter, lfilter_zi, bode
from .filter_interface import *


class ButterworthFilter(FilterInterface):

    def __init__(self, sample_frequency, filter_order=5, frequency_low=0, frequency_high=0):
        self.sample_frequency = sample_frequency
        self.filter_order = filter_order
        self.frequency_low = frequency_low
        self.frequency_high = frequency_high
        self.sample_frequency = sample_frequency
        self.b, self.a = self.get_coefficients()
        self.z = lfilter_zi(self.b, self.a)

    def get_coefficients(self):
        nyq = 0.5 * self.sample_frequency
        low = self.frequency_low / nyq
        high = self.frequency_high / nyq
        b, a = butter(self.filter_order, [low, high], btype='band')
        return b, a

    # overriding abstract method
    def filter_all(self, samples):
        return lfilter(self.b, self.a, samples)

    # overriding abstract method
    def filter_sample(self, sample):
        filtered_sample, self.z = lfilter(self.b, self.a, [sample], zi=self.z)
        return filtered_sample

    # overriding abstract method
    def get_phase(self):
        w, mag, phase = bode((self.b, self.a))
        return phase