from abc import ABC, abstractmethod


class FilterInterface(ABC):

    # Filter all sample at once
    @abstractmethod
    def filter_all(self, samples):
        pass

    # Filter each sample individuality
    @abstractmethod
    def filter_sample(self, sample):
        pass

    @abstractmethod
    def get_phase(self):
        pass