from .data_frame_interface import DataFrameInterface
from ..data_sources.data_source_interface import ECGDataSourceInterface
from ..filters.filter_interface import FilterInterface
from ..filters.butterworth_filter import ButterworthFilter
import numpy


class ECGDataFrame(DataFrameInterface):
    def __init__(self, data_source: ECGDataSourceInterface,
                 sample_frequency: int,
                 gain: float = 1,
                 adc_resolution_bits: int = 0,
                 reference_voltage_mv: float = 1.0,
                 invert=False,
                 filter_low_hz: float = 0,
                 filter_high_hz: float = 0):
        self.data_source = data_source
        self.sample_frequency = sample_frequency
        self.samples = list()
        self.gain: float = gain
        self.adc_resolution_bits: int = adc_resolution_bits
        self.reference_voltage_mv: float = reference_voltage_mv
        self.mv_per_bit = float((self.reference_voltage_mv / pow(2, self.adc_resolution_bits)) * self.gain)
        self.invert = invert
        self.filter_low_hz = filter_low_hz
        self.filter_high_hz = filter_high_hz

        self.pre_filter: FilterInterface = None

        # In case if you need to remove 50 Hz mains interference noise or something else
        if self.filter_low_hz != 0 and self.filter_high_hz != 0:
            self.pre_filter = ButterworthFilter(sample_frequency,
                                                filter_order=5,
                                                frequency_low=filter_low_hz,
                                                frequency_high=filter_high_hz)

    def get_samples(self) -> numpy.array:
        samples_array = self.data_source.read_data()

        if self.mv_per_bit != 1.0:
            samples_array = numpy.multiply(samples_array, self.mv_per_bit)

        if self.invert:
            samples_array = numpy.negative(samples_array)

        if self.filter_low_hz != 0 and self.filter_high_hz != 0:
            return self.pre_filter.filter_all(samples_array)
        else:
            return samples_array

    def get_sampling_frequency(self):
        return self.sample_frequency
