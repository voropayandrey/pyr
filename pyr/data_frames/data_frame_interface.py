from abc import ABC, abstractmethod
import numpy


class DataFrameInterface(ABC):

    @abstractmethod
    def get_samples(self) -> numpy.array:
        pass

    @abstractmethod
    def get_sampling_frequency(self):
        pass
