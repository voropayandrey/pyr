from abc import ABC, abstractmethod
from ..data_frames.data_frame_interface import DataFrameInterface


class PeakDetectorInterface(ABC):

    @abstractmethod
    def __init__(self, data_frame: DataFrameInterface):
        pass

    @abstractmethod
    def detect_sample(self, sample):
        pass

    @abstractmethod
    def get_intervals(self) -> list:
        pass

    @abstractmethod
    def clear(self):
        pass

    @abstractmethod
    def plot(self):
        pass