from .detector_interface import PeakDetectorInterface
from ..data_frames.data_frame_interface import DataFrameInterface
from ..filters.filter_interface import FilterInterface
from ..filters.butterworth_filter import ButterworthFilter
import numpy as np
import math
from ..utils import *
from ..detectors.detector_processing_history import DetectorProcessingHistory
import matplotlib
import matplotlib.pyplot as plt

class ModifiedPanTompkinsDetector(PeakDetectorInterface):
    def __init__(self,
                 data_frame: DataFrameInterface,
                 window_width_percent=10,
                 mean_window_width_percent=200,
                 threshold_height_percent=50,
                 debounce_percent=20,
                 backsearch_enabled=False,
                 history_enabled=False,
                 snr_enabled=False,
                 minimal_snr=3.0,
                 # Don't use in try realtime mode
                 data_length=0):
        self.data_frame = data_frame
        self.window_width_samples = int(window_width_percent/100.0 * data_frame.sample_frequency)
        self.mean_window_width_samples = int(mean_window_width_percent/100.0 * data_frame.sample_frequency)
        self.threshold_height_1 = threshold_height_percent/100.0
        self.threshold_height_2 = self.threshold_height_1
        self.debounce_samples = int(debounce_percent/100.0 * data_frame.sample_frequency)
        self.backsearch_enabled = backsearch_enabled
        self.history_enabled = history_enabled
        self.snr_enabled = snr_enabled
        self.minimal_snr = minimal_snr

        self.snr_history = list()

        self.peak_marker_list_1 = list()
        self.peak_marker_list_2 = list()
        self.peak_marker_list_3 = list()
        self.peak_marker_list_3_length = 0

        self.ecgRaw = list()

        self.filter5_21 = ButterworthFilter(data_frame.sample_frequency,
                                            filter_order=5,
                                            frequency_low=5,
                                            frequency_high=21)

        self.filter_time_shift_samples = len(self.filter5_21.b)


        self.filteredEcg = np.zeros(data_length)
        self.filteredEcgAverage = np.zeros(data_length)
        self.filteredEcgSquared = np.zeros(data_length)
        self.filteredEcgSquaredAverage = np.zeros(data_length)
        self.clear()

        if self.history_enabled:
            self.history_1 = DetectorProcessingHistory(data_length=data_length)
            self.history_2 = DetectorProcessingHistory(data_length=data_length)


    def search_amplitude_max(self, array, start_index, end_index, found_index=0):
        assert found_index >= 0

        if found_index != 0:
            found_max_value = array[found_index]
            found_max_index = found_index
        else:
            found_max_value = array[start_index]
            found_max_index = start_index

        for index in range(start_index, end_index):
            if start_index >= 0 and index < len(array) and array[index] > found_max_value:
                found_max_value = array[index]
                found_max_index = index
        return found_max_value, found_max_index

    def search_amplitude_min(self, array, start_index, end_index, found_index=0):
        assert found_index >= 0

        if found_index != 0:
            found_min_value = array[found_index]
            found_min_index = found_index
        else:
            found_min_value = array[start_index]
            found_min_index = start_index

        for index in range(start_index, end_index):
            if start_index >= 0 and index < len(array) and array[index] < found_min_value:
                found_min_value = array[index]
                found_min_index = index
        return found_min_value, found_min_index

    def add_final_interval(self, index, range=0) -> bool:
        if index - range > 0:
            if find_value_in(self.peak_marker_list_3, index, range):
                return False
            else:
                self.peak_marker_list_3.append(index)
                return True
        else:
            return False

    def detect_sample(self, sample):
        self.new_peak_added_1 = False
        self.new_peak_added_2 = False

        self.ecgRaw.append(sample)

        self.history_1.store_raw_sample(sample)
        self.history_2.store_raw_sample(sample)

        filtered_sample = self.filter5_21.filter_sample(sample)
        self.filteredEcg[self.sample_index_current] = filtered_sample
        filtered_and_squared_sample = pow(filtered_sample, 2)
        self.filteredEcgSquared[self.sample_index_current] = filtered_and_squared_sample

        self.history_1.store_filtered_sample(filtered_sample)
        self.history_2.store_filtered_sample(filtered_and_squared_sample)

        window_index_to = self.sample_index_current
        if self.sample_index_current >= self.window_width_samples:
            window_index_from = self.sample_index_current - self.window_width_samples

            self.windows_sum_1 = 0
            self.windows_sum_2 = 0
            for window_index in range(window_index_from, window_index_to):
                self.windows_sum_1 += self.filteredEcgSquared[window_index]
                self.windows_sum_2 += abs(self.filteredEcg[window_index])

            filtered_squared_mean = self.windows_sum_1 / self.window_width_samples
            self.filteredEcgSquaredAverage[self.sample_index_current] = filtered_squared_mean

            filtered_mean = (self.windows_sum_2 / self.window_width_samples)
            self.filteredEcgAverage[self.sample_index_current] = filtered_mean

            self.history_1.store_intensity_sample(filtered_mean)
            self.history_2.store_intensity_sample(filtered_squared_mean)

        if self.sample_mean_window_index < self.mean_window_width_samples - 1:
            self.sample_mean_window_index += 1
        else:
            self.sample_mean_window_index = 0

            mean_window_index_from = 0
            mean_window_index_to = self.sample_index_current
            if self.sample_index_current >= self.mean_window_width_samples:
                mean_window_index_from = self.sample_index_current - self.mean_window_width_samples

            self.max_previous_1 = self.max_in_window_1
            self.max_previous_2 = self.max_in_window_2
            self.max_in_window_1 = 0
            self.max_in_window_2 = 0
            self.mean_in_window_sum_1 = 0
            self.mean_in_window_sum_2 = 0
            for mean_window_index in range(mean_window_index_from, mean_window_index_to):
                self.mean_in_window_sum_1 += self.filteredEcgAverage[mean_window_index]
                self.mean_in_window_sum_2 += self.filteredEcgSquaredAverage[mean_window_index]

                if self.filteredEcgAverage[mean_window_index] > self.max_in_window_1:
                    if self.max_in_window_1 == 0:
                        self.max_in_window_1 = self.filteredEcgAverage[mean_window_index]
                    else:
                        self.max_in_window_1 = (self.max_in_window_1 + self.filteredEcgAverage[mean_window_index]) / 2

                if self.filteredEcgSquaredAverage[mean_window_index] > self.max_in_window_2:
                    if self.max_in_window_2 == 0:
                        self.max_in_window_2 = self.filteredEcgSquaredAverage[mean_window_index]
                    else:
                        self.max_in_window_2 = (self.max_in_window_2 + self.filteredEcgSquaredAverage[mean_window_index]) / 2

            if self.mean_in_window_1 == 0:
                self.mean_in_window_1 = self.mean_in_window_sum_1 / self.mean_window_width_samples
            else:
                self.mean_in_window_1 = (self.mean_in_window_1 + (self.mean_in_window_sum_1 / self.mean_window_width_samples)) / 2

            if self.mean_in_window_2 == 0:
                self.mean_in_window_2 = self.mean_in_window_sum_2 / self.mean_window_width_samples
            else:
                self.mean_in_window_2 = (self.mean_in_window_2 + (self.mean_in_window_sum_2 / self.mean_window_width_samples)) / 2

            if self.threshold_current_1 == 0:
                self.threshold_current_1 = (((self.max_in_window_1 - self.mean_in_window_1) * self.threshold_height_1) + self.mean_in_window_1)
            else:
                self.threshold_current_1 = (self.threshold_current_1 + (((self.max_in_window_1 - self.mean_in_window_1) * self.threshold_height_1) + self.mean_in_window_1)) / 2

            if self.threshold_current_2 == 0:
                self.threshold_current_2 = (((self.max_in_window_2 - self.mean_in_window_2) * self.threshold_height_2) + self.mean_in_window_2)
            else:
                self.threshold_current_2 = (self.threshold_current_2 + (((self.max_in_window_2 - self.mean_in_window_2) * self.threshold_height_2) + self.mean_in_window_2)) / 2

            for mean_window_index in range(mean_window_index_from, mean_window_index_to):

                if self.debounce_counter_1 > 0:
                    self.debounce_counter_1 -= 1
                else:
                    if self.filteredEcgAverage[mean_window_index] > self.threshold_current_1:
                        self.peak_marker_list_1.append(mean_window_index)
                        self.new_peak_added_1 = True
                        self.debounce_counter_1 = self.debounce_samples

                if self.debounce_counter_2 > 0:
                    self.debounce_counter_2 -= 1
                else:
                    if self.filteredEcgSquaredAverage[mean_window_index] > self.threshold_current_2:
                        self.peak_marker_list_2.append(mean_window_index)
                        self.new_peak_added_2 = True
                        self.debounce_counter_2 = self.debounce_samples

                if self.new_peak_added_1:
                    last_found_peak_index_1 = self.peak_marker_list_1[len(self.peak_marker_list_1) - 1]
                    from_index_1 = last_found_peak_index_1 - int(self.data_frame.sample_frequency / 8)
                    to_index_1 = last_found_peak_index_1
                    found_max_value_1, found_max_index_1 = self.search_amplitude_max(self.ecgRaw, from_index_1,
                                                                                     to_index_1, 0)
                    final_peak_added = self.add_final_interval(found_max_index_1, self.data_frame.sample_frequency / 4)
                    self.new_peak_added_1 = False

                if self.new_peak_added_2:
                    last_found_peak_index_2 = self.peak_marker_list_2[len(self.peak_marker_list_2) - 1]
                    from_index_2 = last_found_peak_index_2 - int(self.data_frame.sample_frequency / 4)
                    to_index_2 = last_found_peak_index_2
                    found_max_value_2, found_max_index_2 = self.search_amplitude_max(self.ecgRaw, from_index_2, to_index_2, 0)
                    final_peak_added = self.add_final_interval(found_max_index_2, self.data_frame.sample_frequency / 4)
                    self.new_peak_added_2 = False

                if self.history_enabled:
                    # If history is enabled save main variables
                    self.history_1.store_mean(self.mean_in_window_1)
                    self.history_2.store_mean(self.mean_in_window_2)
                    self.history_1.store_max(self.max_in_window_1)
                    self.history_2.store_max(self.max_in_window_2)
                    self.history_1.store_threshold(self.threshold_current_1)
                    self.history_2.store_threshold(self.threshold_current_2)

        if self.snr_enabled and len(self.peak_marker_list_3) > 1 and len(
                self.peak_marker_list_3) != self.peak_marker_list_3_length:
            self.peak_marker_list_3_length = len(self.peak_marker_list_3)
            previous_interval_index = self.peak_marker_list_3[len(self.peak_marker_list_3) - 2]
            last_interval_index = self.peak_marker_list_3[len(self.peak_marker_list_3) - 1]
            interval_between = last_interval_index - previous_interval_index

            signal_start_index = previous_interval_index - int(interval_between * 0.5)
            signal_end_index = previous_interval_index + int(interval_between * 0.5)

            noise_start_index = signal_end_index
            noise_end_index = last_interval_index - int(interval_between * 0.25)

            signal_amplitude_max, signal_index_max = self.search_amplitude_max(self.ecgRaw, signal_start_index,
                                                                               signal_end_index)
            noise_amplitude_max, noise_index_max = self.search_amplitude_max(self.ecgRaw, noise_start_index,
                                                                             noise_end_index)
            signal_amplitude_min, signal_index_min = self.search_amplitude_min(self.ecgRaw, signal_start_index,
                                                                               signal_end_index)
            noise_amplitude_min, noise_index_min = self.search_amplitude_min(self.ecgRaw, noise_start_index,
                                                                             noise_end_index)
            signal_amplitude = abs(signal_amplitude_max) + abs(signal_amplitude_min)
            noise_amplitude = abs(noise_amplitude_max) + abs(noise_amplitude_min)
            snr_value = math.log10(signal_amplitude / noise_amplitude) * 20
            if snr_value >= self.minimal_snr:
                self.snr_history.append(snr_value)

        self.last_filtered_sample = self.filteredEcgAverage[self.sample_index_current]
        self.sample_index_current += 1

    def get_intervals(self) -> list:
        intervals_ms = list()
        for i in range(1, len(self.peak_marker_list_3)):
            intervals_ms.append(((self.peak_marker_list_3[i] - self.peak_marker_list_3[
                i - 1]) / self.data_frame.sample_frequency) * 1000)
        return intervals_ms

    def get_snr_total(self):
        if len(self.snr_history) > 1:
            sum = 0
            for snr in self.snr_history:
                sum += snr
            return sum/len(self.snr_history)
        return 0

    def get_snr_total(self, keep_false_snr=True):
        if len(self.snr_history) > 1:
            sum = 0
            false_snr_counter = 0
            for snr in self.snr_history:
                if keep_false_snr or snr > 0:
                    sum += snr
                else:
                    false_snr_counter += 1
            return sum/(len(self.snr_history) - false_snr_counter)
        return 0

    def get_max_snr_(self):
        if len(self.snr_history) > 1:
            return max(self.snr_history)
        return 0

    def get_min_snr_(self):
        if len(self.snr_history) > 1:
            return min(self.snr_history)
        return 0
    def get_mean_snr(self):
        sum = 0
        for snr in self.snr_history:
            sum += snr
        return sum/len(self.snr_history)
    def get_standart_deviation_snr(self):
        mean = self.get_mean_snr()
        sum = 0
        for snr in self.snr_history:
            sum += pow(snr - mean, 2)
        if len(self.snr_history) > 1:
            return math.sqrt(sum/(len(self.snr_history) - 1))
        else:
            return 0
    def get_standart_error_snr(self):
        sd = self.get_standart_deviation_snr()
        return sd/math.sqrt(len(self.snr_history))
    def get_confidential_interval_snr(self, t):
        return self.get_standart_error_snr() * t
    def get_confidential_interval_snr_95(self):
        return self.get_confidential_interval_snr(1.96)

    def clear(self):
        self.snr_history.clear()

        self.sample_index_current = 0
        self.sample_windows_index_current = 0
        self.sample_mean_window_index = 0
        self.windows_sum = 0

        self.threshold_current_1 = 0.0
        self.threshold_current_2 = 0.0

        self.max_value_history_1 = None
        self.max_value_history_2 = None
        self.mean_value_history_1 = None
        self.mean_value_history_2 = None
        self.threshold_value_history_1 = None
        self.threshold_value_history_2 = None

        self.peak_marker_list_1.clear()
        self.peak_marker_list_2.clear()
        self.peak_marker_list_3.clear()

        self.peak_counter_1 = 0
        self.peak_counter_2 = 0
        self.peak_counter_3 = 0

        self.peak_detected_1 = False
        self.peak_detected_2 = False
        self.peak_detected_3 = False

        self.peak_detected_index_1 = 0
        self.peak_detected_index_2 = 0
        self.peak_detected_index_3 = 0

        self.new_peak_added_1 = False
        self.new_peak_added_2 = False

        self.debounce_counter_1 = 0
        self.debounce_counter_2 = 0

        self.last_filtered_sample = 0

        self.mean_in_window_sum_1 = 0
        self.mean_in_window_sum_2 = 0
        self.max_in_window_1 = 0
        self.max_in_window_2 = 0
        self.mean_in_window_1 = 0
        self.mean_in_window_2 = 0

        self.debounce_counter_1 = self.debounce_samples
        self.debounce_counter_2 = self.debounce_samples

    def plot(self):
        length = len(self.history_1.raw_samples_history)

        t_list = list(range(0, length))
        t_np = np.array(t_list)
        t_np = t_np * 1.0 / self.data_frame.get_sampling_frequency()

        peak_markers_1 = np.empty(length)
        peak_markers_1[:] = np.NaN
        for peak_index in self.peak_marker_list_1:
            peak_markers_1[peak_index] = self.filteredEcg[peak_index]

        peak_markers_2 = np.empty(length)
        peak_markers_2[:] = np.NaN
        for peak_index in self.peak_marker_list_2:
            peak_markers_2[peak_index] = self.filteredEcgSquaredAverage[peak_index]

        peak_markers_3 = np.empty(length)
        peak_markers_3[:] = np.NaN
        for peak_index in self.peak_marker_list_3:
            peak_markers_3[peak_index] = self.ecgRaw[peak_index]

        intervals = self.get_intervals()
        intervals_t = list()
        sum = 0
        for interval in intervals:
            intervals_t.append(sum)
            sum += interval / 1000

        intervalFig = plt.figure(1)
        plt.plot(intervals_t, intervals, '#0000FF', label='Intervals', linewidth='1')
        plt.xlabel('Time, s')
        plt.ylabel('Interval, ms')

        if plt.get_backend() == 'Qt5Agg':
            figManager = plt.get_current_fig_manager()
            figManager.window.showMaximized()

        fig, axs = plt.subplots(nrows=3, ncols=1, sharex='all')

        self.history_1.plot(axs, 0, t_np)
        axs[0].plot(t_np, peak_markers_1, '-ro', label='RR peak', linewidth='1')
        axs[0].set_xlabel('Time, s')
        axs[0].set_ylabel('Voltage, mV')
        axs[0].grid(True)
        axs[0].legend(loc='best')

        self.history_2.plot(axs, 1, t_np)
        axs[1].plot(t_np, peak_markers_2, '-ro', label='RR peak', linewidth='1')
        axs[1].set_xlabel('Time, s')
        axs[1].set_ylabel('Voltage, mV')
        axs[1].grid(True)
        axs[1].legend(loc='best')

        axs[2].plot(t_np, self.ecgRaw, '#0000FF', label='Raw ECG', linewidth='1')
        axs[2].plot(t_np, peak_markers_3, '-ro', label='RR peak', linewidth='1')
        axs[2].set_xlabel('Time, s')
        axs[2].set_ylabel('Voltage, mV')
        axs[2].grid(True)
        axs[2].legend(loc='best')

        if plt.get_backend() == 'Qt5Agg':
            figManager = plt.get_current_fig_manager()
            figManager.window.showMaximized()
        plt.show()