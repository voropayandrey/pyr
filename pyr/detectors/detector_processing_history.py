
from enum import Enum
import numpy

class ProcessingMode(Enum):
    # In simulated real-time we have information about data length that will be processed.
    # We can use numpy arrays to speed up the processing a bit.
    SIMULATED_REAL_TIME = 0
    # In real-time mode we have no idea about length of incoming data, so we can't use static arrays.
    TRUE_REAL_TIME = 1

class DetectorProcessingHistory:
    def __init__(self, data_length: int = 0):

        self.data_length = data_length
        if data_length == 0:
            self.mode = ProcessingMode.TRUE_REAL_TIME
        else:
            self.mode = ProcessingMode.SIMULATED_REAL_TIME

        self.raw_samples_history_index = 0
        self.filtered_samples_history_index = 0
        self.intensity_samples_history_index = 0
        self.threshold_history_index = 0
        self.max_history_index = 0
        self.min_history_index = 0
        self.mean_history_index = 0

        if self.mode == ProcessingMode.SIMULATED_REAL_TIME:
            #TODO or throw an exception, in simulated real-time mode data length should be known
            assert self.data_length > 0
            self.raw_samples_history = numpy.zeros(self.data_length)
            self.filtered_samples_history = numpy.zeros(self.data_length)
            self.intensity_samples_history = numpy.zeros(self.data_length)
            self.threshold_history = numpy.zeros(self.data_length)
            self.max_history = numpy.zeros(self.data_length)
            self.min_history = numpy.zeros(self.data_length)
            self.mean_history = numpy.zeros(self.data_length)

        elif self.mode == ProcessingMode.TRUE_REAL_TIME:
            # Data length is unknown, using lists instead of arrays
            self.raw_samples_history = list()
            self.filtered_samples_history = list()
            self.intensity_samples_history = list()
            self.threshold_history = list()
            self.max_history = list()
            self.min_history = list()
            self.mean_history = list()

    # def __store_sample(self, store_to, index, value):
    #     if self.mode == ProcessingMode.SIMULATED_REAL_TIME:
    #         self.store_to[self.raw_samples_history] = raw_sample
    #         self.raw_samples_history += 1
    #     elif self.mode == ProcessingMode.TRY_REAL_TIME:

    def store_raw_sample(self, value):
        if self.mode == ProcessingMode.SIMULATED_REAL_TIME:
            self.raw_samples_history[self.raw_samples_history_index] = value
        elif self.mode == ProcessingMode.TRUE_REAL_TIME:
            self.raw_samples_history.append(value)
        self.raw_samples_history_index += 1

    def store_filtered_sample(self, value):
        if self.mode == ProcessingMode.SIMULATED_REAL_TIME:
            self.filtered_samples_history[self.filtered_samples_history_index] = value
        elif self.mode == ProcessingMode.TRUE_REAL_TIME:
            self.filtered_samples_history.append(value)
        self.filtered_samples_history_index += 1

    def store_intensity_sample(self, value):
        if self.mode == ProcessingMode.SIMULATED_REAL_TIME:
            self.intensity_samples_history[self.intensity_samples_history_index] = value
        elif self.mode == ProcessingMode.TRUE_REAL_TIME:
            self.intensity_samples_history.append(value)
        self.intensity_samples_history_index += 1

    def store_threshold(self, value):
        if self.mode == ProcessingMode.SIMULATED_REAL_TIME:
            self.threshold_history[self.threshold_history_index] = value
        elif self.mode == ProcessingMode.TRUE_REAL_TIME:
            self.threshold_history.append(value)
        self.threshold_history_index += 1

    def store_max(self, value):
        if self.mode == ProcessingMode.SIMULATED_REAL_TIME:
            self.max_history[self.max_history_index] = value
        elif self.mode == ProcessingMode.TRUE_REAL_TIME:
            self.max_history.append(value)
        self.max_history_index += 1

    def store_min(self, value):
        if self.mode == ProcessingMode.SIMULATED_REAL_TIME:
            self.min_history[self.max_history_index] = value
        elif self.mode == ProcessingMode.TRUE_REAL_TIME:
            self.min_history.append(value)
        self.max_history_index += 1

    def store_mean(self, value):
        if self.mode == ProcessingMode.SIMULATED_REAL_TIME:
            self.mean_history[self.mean_history_index] = value
        elif self.mode == ProcessingMode.TRUE_REAL_TIME:
            self.mean_history.append(value)
        self.mean_history_index += 1

    def plot(self, axis, index, x):
        axis[index].plot(x, self.raw_samples_history, '#000000', label='Raw ECG', linewidth='1', alpha=0.5)
        axis[index].plot(x, self.filtered_samples_history, '#0000FF', label='Filtered ECG', linewidth='1')
        axis[index].plot(x, self.intensity_samples_history, '#FF00FF', label='Intensity', linewidth='1')
        axis[index].plot(x, self.mean_history, '#FFFF00', label='mean_value_history_1')
        axis[index].plot(x, self.max_history, '#FF0000', label='max_value_history_1')
        axis[index].plot(x, self.threshold_history, '#00FF00', label='threshold_value_history_1')